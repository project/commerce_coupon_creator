<?php

/**
 * Implements hook_views_data().
 */
function aba_coupons_views_data() {
  $data['views']['table']['group'] = t('Custom Global');
  
  $data['views']['coupon_info_views_field'] = [
    'title' => 'Informacion de cupón',
    'help' => 'Informacion de cupón..',
    'field' => [
      'id' => 'coupon_info_views_field',
    ],
  ];
  return $data;
}
