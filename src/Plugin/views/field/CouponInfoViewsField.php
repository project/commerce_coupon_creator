<?php

namespace Drupal\aba_coupons\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Core\Url;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("coupon_info_views_field")
 */
class CouponInfoViewsField extends FieldPluginBase {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // First check whether the field should be hidden if the value(hide_alter_empty = TRUE) /the rewrite is empty (hide_alter_empty = FALSE).
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $coupon = $values->_entity;
    $coupon_code = $coupon->getCode();
    $coupon_sent = \Drupal::database()->query(
      "SELECT * FROM {aba_coupons} WHERE coupon_code = :code",
      [':code' => $coupon_code,]
    )->fetchAll();
    if (!$coupon_sent) {
      $info = [
        '#type' => 'markup',
        '#markup' => "Aun no ha sido compartido.",
      ];
    } else {
      $info = [
        '#type' => 'markup',
        '#markup' => "Cupon compartido con <strong>" . $coupon_sent[0]->sent_to . '.</strong>',
      ];
    }
    return [
      '#type' => 'container',
      '#attributes' => [
        'id' => [
          'coupon-info-wrapper-' . $coupon_code,
        ],
      ],
      'info' => $info,
    ];
    
  }

}