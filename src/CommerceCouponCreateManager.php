<?php

namespace Drupal\commerce_coupon_create;

use Drupal\commerce_promotion\CouponCodePattern;
use Drupal\commerce_promotion\CouponCodeGeneratorInterface;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Drupal\Core\Url;

/**
 * Class BatchService.
 */
class CommerceCouponCreateManager {

  /**
   * Class constructor.
   *
   */
  public function __construct() {
    //$this->stockServiceManager = \Drupal::service('commerce_stock.service_manager');
  }

  public function createCoupons($promotion_id, $quantity, $usage_limit, $usage_limit_customer) {
    $coupon_values = [
      'promotion_id' => $promotion_id,
      'usage_limit' => $usage_limit,
      'usage_limit_customer' => $usage_limit_customer,
    ];
    $pattern = new CouponCodePattern(
      'alphanumeric', 
      'abasty-', // prefix
      '', // suffix
      3, // lenght
    );
    $coupon_storage = \Drupal::entityTypeManager()->getStorage('commerce_promotion_coupon');
    $coupon_code_generator = \Drupal::service('commerce_promotion.coupon_code_generator');
    $codes = $coupon_code_generator->generateCodes($pattern, $quantity/* limit */);
    $created_coupons = [];
    if (!empty($codes)) {
      foreach ($codes as $code) {
        $coupon = $coupon_storage->create([
            'code' => $code,
          ] + $coupon_values);
        $coupon->save();
        $created_coupons[$code] = $coupon;
      }
    }
    else {
      
    }
    return $created_coupons;


  }

  public function getModuleFilesPath() {
    //$root_path = \Drupal::root() . '/';
    return \Drupal::service('module_handler')->getModule('commerce_coupon_create')->getPath() . '/files';
  }

  public function getModulePath() {
    //$root_path = \Drupal::root() . '/';
    return \Drupal::service('module_handler')->getModule('commerce_coupon_create')->getPath();
  }

  public function getCouponsPath($filename) {
    $root_path = \Drupal::root() . '/';
    return $root_path . $this->getModulePath() . '/coupons/' . $filename;
  }

  public function getCouponImageUrl($code) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $front_path = $host . Url::fromRoute('<front>', [])->toString();
    return $front_path . '/' . \Drupal::service('module_handler')->getModule('commerce_coupon_create')->getPath() . '/files/coupons/' . $code . '.jpeg';

    /*$host = \Drupal::request()->getSchemeAndHttpHost();
    $front_path = $host . Url::fromRoute('<front>', [])->toString();
    $coupon_url = $front_path . "cupon/view/" . $code;
    return $coupon_url;*/
  }

  public function getCouponViewUrl($code) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $front_path = $host . Url::fromRoute('<front>', [])->toString();
    $coupon_url = $front_path . "cupon/view/" . $code;
    return $coupon_url;
  }

  public function getCouponApplyUrl($code) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $front_path = $host . Url::fromRoute('<front>', [])->toString();
    $coupon_url = $front_path . "cupon/" . $code;
    return $coupon_url;
  }

  public function generateImage($coupon) {
    $fields = [];
    $promotion = $coupon->getPromotion();
    $code = $coupon->getCode();
    $fields['coupon_code'] = $code; 
    $created = date('Y/m/d H:i', $coupon->getCreatedTime());
    $fields['created'] = $created;
    $usageLimit = $coupon->getUsageLimit();
    $fields['usage_limit'] = $usageLimit;
    $customerUsageLimit = $coupon->getCustomerUsageLimit();
    $fields['customer_usage_limit'] = $customerUsageLimit;
    $endate = $promotion->getEndDate('GMT+6');
    $expires = $endate? $endate->format('Y/m/d H:i') : '-';
    $fields['expires'] = $expires;
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $front_path = $host . Url::fromRoute('<front>', [])->toString();
    $frame_file = 'aba-coupon-frame-' . $promotion->id() . '.jpeg';
    $couponFramePath = $this->getModulePath() . '/images/' . $frame_file;//'/coupon-frame-blue-test.jpeg';
    $image = imagecreatefromjpeg($couponFramePath);
    
    $targetPath = $this->getCouponsPath($code . '.jpeg');
    $x = 12;
    $y = 130;
    $line_height = 13;
    $font_size = 2;
    // Agregar codigo de cupon grande
    $colors = [
      'white' => imagecolorallocate($image, 255, 255, 255),
      'black' => imagecolorallocate($image, 0, 0, 0)
    ];
    
    $fontPath = $this->getModulePath() . '/fonts/Poppins-Regular.ttf';
    //ksm($fontPath);
    //$poppins = imageloadfont($fontPath);

    $coupon_layout = '[
      {"field":"coupon_code", "label":"", "x":510, "y":30, "size":9, "color":"white"},
      {"field":"coupon_code", "label":"ID", "x":12, "y":125, "size":2, "color":"black"},
      {"field":"created", "label":"Emited", "x":12, "y":141, "size":2, "color":"black"},
      {"field":"expires", "label":"Expires", "x":12, "y":157, "size":2, "color":"black"},
      {"field":"usage_limit", "label":"Number of uses", "x":12, "y":173, "size":2, "color":"black"},
      {"field":"customer_usage_limit", "label":"Uses per client", "x":12, "y":187, "size":2, "color":"black"}
    ]';
    /*[
      ["field"=>"coupon_code", "label"=>"ID", "x"=>440, "y"=>44, "size"=>16, "color"=>"white"],
      ["field"=>"coupon_code", "label"=>"ID", "x"=>12, "y"=>130, "size"=>12, "color"=>"black"],
    ];
    */

    $coupon_values = json_decode($coupon_layout);
    ksm($coupon_values);
    
    foreach($coupon_values as $val) {
      $label = (isset($val->label) && $val->label != '')? $val->label . ': ' . $fields[$val->field] 
        : $fields[$val->field];
      //imagettftext($image, 
        //$val->size, /*size*/
        //0, /*angle */
        //$val->x, /*x */
        //$val->y, /*y */
        //$colors[$val->color], /*color */ 
        //'', //$fontPath, /*font filename */
        //$label /*text */
      //);
      imagestring($image, 
        $val->size /*font size*/, 
        $val->x /*x*/, 
        $val->y /*y*/, 
        $label, 
        $colors[$val->color]
      );
    }

    // Agregar codigo de cupon info cupon
    //$color = imagecolorallocate($image, 4, 0, 28);
    //imagestring(
      //$image, 
      //$font_size /*font size*/, 
      //$x /*x*/, 
      //$y /*y*/, 
      //'ID: ' . $code /*string*/, 
      //$color
    //);
    // Agregar fecha de creacion
    //imagestring(
      //$image, 
      //$font_size /*font size*/, 
      //$x /*x*/, 
      //$y + $line_height /*y*/, 
      //'Creado: ' . $created, 
      //$color
    //);
    // Agregar fecha de expiracion
    //imagestring(
      //$image, 
      //$font_size /*font size*/, 
      //$x /*x*/, 
      //$y + $line_height * 2 /*y*/, 
      //'Expira: ' . $expires, 
      //$color
    //);
    //imagestring(
      //$image, 
      //$font_size /*font size*/, 
      //$x /*x*/, 
      //$y + $line_height * 3 /*y*/, 
      //'Numero de usos: ' . $usageLimit, 
      //$color
    //);
    //imagestring(
      //$image, 
      //$font_size /*font size*/, 
      //$x /*x*/, 
      //$y + $line_height * 4 /*y*/, 
      //'Usos x cliente: ' . $customerUsageLimit, 
      //$color
    //);
    
    // Generar QR
    $qrpath = $this->getCouponsPath('qr.' . $code . '.png');
    $link = $front_path . '/cupon/' . $code;
    $renderer = new ImageRenderer(
      new RendererStyle(100),
      new ImagickImageBackEnd()
    );
    
    $writer = new Writer($renderer);
    $result = $writer->writeFile($link, $qrpath);
    $qr = imagecreatefrompng($qrpath);
    // Copy and merge
    imagecopymerge($image, $qr, 32, 18, 0, 0, 100, 100, 100);
    imagealphablending($image, false);
    imagesavealpha($image, true);
    imagejpeg($image, $targetPath);
    imagedestroy($qr);
    // Output and free from memory
    //header('Content-Type: image/gif');
    //imagegif($dest);
    return $targetPath;
  }

  function getDescuentoCupon($product) {
    ksm($product);
      $promos = $product->get('field_categorias_de_promocion')->getValue();
      ksm($promos);
      
      $catp_id = array_pop($promos)['target_id'];
      $termStorage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');
      $term = $termStorage->load($catp_id);
      $categoria_name = $term->getName();
      $porcentaje_descuento = str_replace('%', '', $categoria_name);
      $precio = $product->get('field_price')->getValue();

      /*$variations = $product->getVariationIds();
      foreach($variations as $vid) {
        $variation = \Drupal\commerce_product\Entity\ProductVariation::load($vid);
        $field_precio = $product_variation->price
*/
      $precio = $precio[0]['number'];
      $descuento = (float)$precio * ((float)$porcentaje_descuento / 100);
      $descuento = '-' . number_format((float)$descuento, 2) . 'Q';
      return $descuento;
    //}
    return false;
  }

}