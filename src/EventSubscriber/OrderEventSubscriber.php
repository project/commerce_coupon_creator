<?php

namespace Drupal\commerce_coupon_create\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_promotion\Event\PromotionEvent;
use Drupal\commerce_promotion\Event\PromotionEvents;
use Drupal\commerce_promotion\Event\CouponEvent;
use Drupal\commerce_promotion\Event\CouponEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Order event subscriber.
 *
 * Applies coupon codes.
 */
class OrderEventSubscriber implements EventSubscriberInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Temp Store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Construct a new OrderEventSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store
   *   Temp store.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   Current route match.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store,
    RouteMatchInterface $current_route_match,
    LoggerChannelInterface $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->tempStore = $temp_store->get('aba_coupons');
    $this->currentRouteMatch = $current_route_match;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      OrderEvents::ORDER_PRESAVE => ['onOrderPresave', -100],
      PromotionEvents::COUPON_CREATE => ['onCouponCreate', -100],
    ];
  }

  /**
   * Event listener for coupon create event.
   *
   * @param Drupal\commerce_promotion\Event\CouponEvent $couponEvent
   *   The promotion event.
   */
  public function onCouponCreate(CouponEvent $couponEvent) {
    $coupon = $couponEvent->getCoupon();
    $couponsManager = \Drupal::service('commerce_coupon_create.coupons_manager');
    $couponsManager->generateImage($coupon);
  }

  /**
   * Event listener for order update event.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $orderEvent
   *   The order event.
   */
  public function onOrderPresave(OrderEvent $orderEvent) {
    $order = $orderEvent->getOrder();

    // Order needs order items and has to be a cart (=draft).
    if ($order->hasItems() && $order->getState()->getId() === 'draft') {
      $route_name = $this->currentRouteMatch->getRouteName();
      
      // Only react it a product was added to the cart.
      // Else the coupon code cannot be overridden in the checkout.
      $valid_routes = [
        'entity.node.canonical',
        'commerce_cart.page',
        'entity.commerce_product.canonical',
        'rest.commerce_cart_collection.GET',
      ];
      if (in_array($route_name, $valid_routes, TRUE)) {
        $this->applyCouponCode($order);
      }
    }
  }


  /**
   * Apply coupon code.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function applyCouponCode(OrderInterface $order) {
    //ksm('applyCouponCode');

    // The customer has to enter the checkout select a shipping
    // method before a shipping is saved to the database.
    $code = $this->tempStore->get('coupon_code');
    //ksm($code);
    if (empty($code)) {
      return;
    }

    /** @var \Drupal\commerce_promotion\Entity\Coupon $coupon */
    $coupon = $this->entityTypeManager->getStorage('commerce_promotion_coupon')->loadEnabledByCode($code);
    //ksm($coupon);

    // The coupon storage returns false if none could be loaded.
    if (!empty($coupon)) {
      // We intentionally only support one coupon code and the link cannot
      // be used in conjunction with any other coupons.
      $order->set('coupons', $coupon->id());
      // We flag the code on the order as the temp store is currently lost when
      // a user logs in. If the user uses the link and then logs in before
      // adding something to their cart, the coupon will not be applied.
      // However, if the user logs in after adding something to their cart the
      // knowledge that the coupon was applied via a link will not be lost.
      // @see: https://www.drupal.org/project/drupal/issues/3308135
      $order->setData('aba_coupon_promotion_code', $code);
      // We can now remove the coupon from the temp store. This prevents it
      // from being used with subsequent orders.
      $this->tempStore->delete('coupon_code');
      $this->logger->info(
        'Commerce Promo Link applied coupon @coupon_id to order ID @order_id.',
        [
          '@coupon_id' => $coupon->id(),
          '@order_id' => $order->id(),
        ]
      );
    }
    else {
      $this->messenger()->addWarning($this->t('Your coupon code @code has expired.', ['@code' => $code]));
      $this->logger->info(
        'Commerce Promo Link could not load coupon code @code. The coupon has likely expired.',
        ['@code' => $code]
      );
    }
  }

}
