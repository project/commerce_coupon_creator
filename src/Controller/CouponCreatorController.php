<?php

namespace Drupal\commerce_coupon_create\Controller;

use Drupal\Core\Controller\ControllerBase;

class CouponCreatorController extends ControllerBase {

  /**
   * Content for the Coupon creator page.
   */
  public function content($commerce_promotion) {
    return [
      '#markup' => $this->t('Editor de cupón'),
    ];
  }

}
